<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	return view('front-end.pages.index');
});

Route::get('/master', function(){
	return view('front-end.theme.index');
});

Route::view('login', 'front-end.theme.login');
Route::view('register', 'front-end.theme.register');

Route::get('/registration', function(){
	return view('front-end.pages.register');
});

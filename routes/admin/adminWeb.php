<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin', function () {	
    return view('admin/login/login');
});

Route::get('/admin/dashboard', 'Admin\DashboardController@index');

Route::get('home', function(){
	return redirect('admin/dashboard');
});
Route::get('/admin/skills/list', 'Admin\SkillsController@index')->name('skills/list');

Route::get('/admin/skills/create', 'Admin\SkillsController@create')->name('skills/create');
Route::post('/admin/skills/create_action', 'Admin\SkillsController@create_action')->name('skills/create_action');

Route::get('/admin/skills/ajaxSkillList', 'AjaxdataController@ajaxSkillList')->name('skills/ajaxSkillList');
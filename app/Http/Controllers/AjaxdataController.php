<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Skills;
use DataTables;

class AjaxdataController extends Controller
{

	function ajaxSkillList()
    {
     	$skills = Skills::select('*');
     	return DataTables::of($skills)->make(true);
    }
    
}

@extends('front-end.master')

@section('css')
	<style type="text/css">
		.job-alert-widget {
      border: 1px solid #f1f1f1;
	    padding: 10px;
	    /* display: inline-block; */
	    margin: 15px 0px;
		}
		.job-alert-widget>.job-alert-title {
			text-align: center;
			font-size: 17px;
	    clear: left;
	    padding: 5px;
	    color: #666;
	    font-weight: 300;
		}
		.job-alert-title h3 {
	    border: none;
	    font-size: 17px;
	    clear: left;
    	color: #666;
    	font-weight: 300;
		}
		.job-alert-button {
			padding: 5px;
		}
		.job-alert-button>a button {
			width:100%;
		}
		.widget-title>h3 {
	    font-size: 0.9em;
	    line-height: 0.8em;
	    font-weight: 500;
		}
		.job-alert-widget ul li .job-count {
			float: right;
		}
		.job-count {
			color: #024c77d1;
		}
		.job-alert-btn {
			background-color: #d81b60;
		}
		.job-alert-btn:hover {
			color: #fff;
		}
	</style>
@stop

@section('header')
	<div class="header">    
	  <!-- Start intro section -->
	  <section id="intro" class="section-intro">
	    <div class="logo-menu" style="border-bottom:1px solid #eee;">
	      <nav class="navbar navbar-default" role="navigation" data-spy="affix" data-offset-top="50">
	        <div class="container">
	          <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
	              <span class="sr-only">Toggle navigation</span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand logo" href="{{ url('/') }}">
	            	<img src="{{ ('assets/front-end/logo/logo.png') }}" alt="" {{-- style="margin-top:10px" --}}>
	            </a>
	          </div>

	          <div class="collapse navbar-collapse" id="navbar">              
	            <!-- Start Navigation List -->
		          <ul class="nav navbar-nav">
		            <li>
		              <a class="active" href="#">Home</a>
		            </li>
		          </ul>
		          <ul class="nav navbar-nav navbar-right float-right">
		        		<li class="btn-m">
		        			<a class="btn btn-md" href="" style="font-size:13px;font-weight:500" data-target="#login" data-toggle="modal"><i class="ti-lock"></i>  Log In</a>
		        		</li>
		            <li class="btn-m">
		            	<a class="btn btn-md" style="font-size:13px;font-weight:500" data-target="#register" data-toggle="modal"><i class="ti-user"></i>  Registration
		            	</a>
		            </li>
		          </ul>
	          </div>                           
	        </div>
	        <!-- Mobile Menu Start -->
	        <ul class="wpb-mobile-menu">
	          <li>
	            <a class="active" href="#">Home</a>
	            <ul>
	              <li><a class="active" href="#">Home 1</a></li>
	              <li><a href="#">Home 2</a></li>
	              <li><a href="#">Home 3</a></li>
	              <li><a href="#">Home 4</a></li>
	            </ul>                       
	          </li>
	          <li class="btn-m"><a href="" data-target="#login" data-toggle="modal">Log In</a></li>          
	          <li class="btn-m"><a href="{{ url('registration') }}">Registration</a></li>
	        </ul>
	        <!-- Mobile Menu End --> 
	      </nav>
	 		</div>
	    <section>
	    	<div class="" style="background-color:#444;">
	    	</div>
	    </section>
	  </section>
	</div>
@stop

@section('content')
	<section class="find-job section">
    <div class="container">
    	<div class="col-xs-12 col-sm-12">
	      <div class="section latest-jobs-ads">
					<div class="section-title tab-manu">
					  <h2 class="pull-left section-title">Create Hiring Profile</h2>
					  <p class="pull-right hidden-xs" style="margin-top: 5px;">Mandatory Fields <b class="text-danger">*</b></p>
						<div class="clearfix" style="border-bottom:1px solid #d81b60;"></div>
					  <form action="/action_page.php" style="margin-top: 20px;">
					    {{-- <div class="col-sm-12" style="margin-top: 20px;"> --}}
					    	<div class="form-group col-sm-6">
						      <label class="control-label" for="account_type" style="padding-right:10px;">Account Type :</label>
						      <div class="">
						        <input type="text" class="form-control" id="account_type" name="account_type" value="{{ 'Candidate' }}" readonly>
						      </div>
						    </div>
						    <div class="form-group col-sm-6">
						      <label class="control-label" for="username" style="padding-right: 10px;"><b class="text-danger"> *</b> Username :</label>
						      <div class="">          
						        <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username">
						      </div>
						    </div>
						  {{-- </div>
						  <div class="col-sm-12"> --}}
						    <div class="form-group col-sm-6">
						      <label class="control-label" for="fname" style="padding-right:10px;"><b class="text-danger"> *</b> First Name :</label>
						      <div class="">
						        <input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname">
						      </div>
						    </div>
						    <div class="form-group col-sm-6">
						      <label class="control-label" for="lname" style="padding-right: 10px;"><b class="text-danger"> *</b> Last Name :</label>
						      <div class="">          
						        <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lname">
						      </div>
						    </div>
						  {{-- </div>
					    <div class="col-sm-12"> --}}
						    <div class="form-group col-sm-6">
						      <label class="control-label" for="mobile" style="padding-right:10px;"><b class="text-danger"> *</b> Mobile No. :</label>
						      <div class="">
						        <input type="text" class="form-control" id="mobile" placeholder="Where recruiter can contact you" name="mobile">
						      </div>
						    </div>
						    <div class="form-group col-sm-6">
						      <label class="control-label" for="email" style="padding-right:10px;"><b class="text-danger"> *</b> Email Id :</label>
						      <div class="">
						        <input type="email" class="form-control" placeholder="Enter your active email id to receive relevant jobs" name="email">
						      </div>
						    </div>
						  {{-- </div>
						  <div class="col-sm-12"> --}}
						    <div class="form-group col-sm-6">
						      <label class="control-label" for="password" style="padding-right: 10px;"><b class="text-danger"> *</b> Password :</label>
						      <div class="">          
						        <input type="password" class="form-control" placeholder="Enter Password" name="password">
						      </div>
						    </div>
						    <div class="form-group col-sm-6">
						      <label class="control-label" for="cpassword" style="padding-right: 10px;"><b class="text-danger"> *</b> Confirm Password :</label>
						      <div class="">          
						        <input type="password" class="form-control" placeholder="Confirm Password" name="cpassword">
						      </div>
						    </div>
						  {{-- </div> --}}
						  <div class="col-sm-12 text-center">
					  		<p><input type="checkbox" name="term" style="margin-right:10px;">I agreed to the <a href="" target="_blank" class="primary-link" style="color: #d81b60;">Terms and Conditions</a> governing the use of Hiring.com.</p>
					  		<div style="margin-bottom: 10px;"></div>
					  		<button type="button" class="btn btn-lg btn-auth">Register Now</button>
						  </div>
					  </form>
					</div>
					<div class="clearfix"></div>
					
				</div>
	    </div>
    </div>
	</section>
@stop

@section('js')
@stop
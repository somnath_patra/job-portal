@extends('front-end.master')

@section('css')
	<style type="text/css">
		.job-alert-widget {
      border: 1px solid #f1f1f1;
	    padding: 10px;
	    /* display: inline-block; */
	    margin: 15px 0px;
		}
		.job-alert-widget>.job-alert-title {
			text-align: center;
			font-size: 17px;
	    clear: left;
	    padding: 5px;
	    color: #666;
	    font-weight: 300;
		}
		.job-alert-title h3 {
	    border: none;
	    font-size: 17px;
	    clear: left;
    	color: #666;
    	font-weight: 300;
		}
		.job-alert-button {
			padding: 5px;
		}
		.job-alert-button>a button {
			width:100%;
		}
		.widget-title>h3 {
	    font-size: 0.9em;
	    line-height: 0.8em;
	    font-weight: 500;
		}
		.job-alert-widget ul li .job-count {
			float: right;
		}
		.job-count {
			color: #024c77d1;
		}
		.job-alert-btn {
			background-color: #d81b60;
		}
		.job-alert-btn:hover {
			color: #fff;
		}
	</style>
@stop


@section('header')
	<div class="header">    
	  <!-- Start intro section -->
	  <section id="intro" class="section-intro">
	    <div class="logo-menu">
	      <nav class="navbar navbar-default" role="navigation" data-spy="affix" data-offset-top="50">
	        <div class="container">
	          <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
	              <span class="sr-only">Toggle navigation</span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand logo" href="{{ url('/') }}">
	            	<img src="{{ ('assets/front-end/logo/logo.png') }}" alt="" {{-- style="margin-top:10px" --}}>
	            </a>
	          </div>

	          <div class="collapse navbar-collapse" id="navbar">              
	            <!-- Start Navigation List -->
	          <ul class="nav navbar-nav">
	            <li>
	              <a class="active" href="#">Home</a>
	            </li>
	          </ul>
	          <ul class="nav navbar-nav navbar-right float-right">
	        		<li class="btn-m"><a class="btn btn-md" href="" style="font-size:13px;font-weight:500" data-target="#login" data-toggle="modal"><i class="ti-lock"></i>  Log In</a></li>
	            <li class="btn-m"><a class="btn btn-md" style="font-size:13px;font-weight:500" data-target="#register" data-toggle="modal"><i class="ti-user"></i>  Registration</i></a></li>
	          </ul>
	          </div>                           
	        </div>
	        <!-- Mobile Menu Start -->
	        <ul class="wpb-mobile-menu">
	          <li>
	            <a class="active" href="#">Home</a>
	            <ul>
	              <li><a class="active" href="#">Home 1</a></li>
	              <li><a href="#">Home 2</a></li>
	              <li><a href="#">Home 3</a></li>
	              <li><a href="#">Home 4</a></li>
	            </ul>                       
	          </li>
	          <li class="btn-m"><a href="" data-target="#login" data-toggle="modal">Log In</a></li>          
	          <li class="btn-m"><a href="{{ url('registration') }}">Registration</a></li>
	        </ul>
	        <!-- Mobile Menu End --> 
	      </nav>
	 		</div>
	    <div class="search-container">
	      <div class="container">
	        <div class="row">
	          <div class="col-md-12">
	            <div class="content">
	              <form method="" action="">
	                <div class="row">
	                  <div class="col-md-4 col-sm-6">
	                    <div class="form-group">
	                      <input class="form-control" type="text" placeholder="job title / keywords / company name">
	                      <i class="ti-time"></i>
	                    </div>
	                  </div>
	                  <div class="col-md-4 col-sm-6">
	                    <div class="form-group">
	                      <input class="form-control" type="email" placeholder="city / province / zip code">
	                      <i class="ti-location-pin"></i>
	                    </div>
	                  </div>
	                  <div class="col-md-3 col-sm-6">
	                    <div class="search-category-container">
	                      <label class="styled-select">
	                        <select class="dropdown-product selectpicker">
	                          <option>All Categories</option>
	                          <option>Finance</option>
	                          <option>IT & Engineering</option>
	                          <option>Education/Training</option>
	                          <option>Art/Design</option>
	                          <option>Sale/Markting</option>
	                          <option>Healthcare</option>
	                          <option>Science</option>                              
	                          <option>Food Services</option>
	                        </select>
	                      </label>
	                    </div>
	                  </div>
	                  <div class="col-md-1 col-sm-6">
	                    <button type="button" class="btn btn-search-icon"><i class="ti-search"></i></button>
	                  </div>
	                </div>
	              </form>
	            </div>
	            <div class="popular-jobs text-center">
	              {{-- <b>Popular Keywords: </b> --}}
	              <a href="#">Web Design</a>
	              <a href="#">Manager</a>
	              <a href="#">Programming</a>
	            </div>
	            {{-- <div class="text-center auth">
	            	<div class="col-sm-4"></div>
	              <div class="col-sm-2 col-xs-6"><a class="btn btn-md btn-auth" href=""><i class="ti-lock"></i>  Log In</a></div>
	              <div class="col-sm-2 col-xs-6"><a class="btn btn-md btn-auth" href=""><i class="ti-lock"></i>  Registration</i></a></div>
	              <div class="col-sm-4"></div>
	            </div> --}}
	          </div>
	        </div>
	      </div>
	    </div>
	  </section>
	</div>
@stop

@section('content')
	<section class="find-job section">
	    <div class="container">
	    	<div class="col-xs-12 col-sm-9">
		      <div class="section latest-jobs-ads">
						<div class="section-title tab-manu">
							<h2 class="pull-left section-title">Latest Jobs</h2>
							 <!-- Nav tabs -->      
							<ul class="nav nav-tabs pull-right" role="tablist">
								<li role="presentation" class=""><a href="#hot-jobs" data-toggle="tab" aria-expanded="false">Hot Jobs</a></li>
								<li role="presentation" class=""><a href="#recent-jobs" data-toggle="tab" aria-expanded="false">Recent Jobs</a></li>
								<li role="presentation" class="active"><a href="#popular-jobs" data-toggle="tab" aria-expanded="true">Popular Jobs</a></li>
							</ul>
						</div>
						<div class="clearfix"></div>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade" id="hot-jobs">
								<div class="row">
					        <div class="col-md-12">
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-1.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Need a web designer</a><span class="full-time">Full-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-brush"></i>Art/Design</a></span>
					                    <span><i class="ti-location-pin"></i>Washington, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-2.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Front-end developer needed</a><span class="full-time">Full-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-desktop"></i>Technologies</a></span>
					                    <span><i class="ti-location-pin"></i>Cupertino, CA, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-3.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Senior Accountant</a><span class="part-time">Part-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-home"></i>Finance</a></span>
					                    <span><i class="ti-location-pin"></i>Delaware, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-4.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Fullstack web developer needed</a><span class="full-time">Full-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-desktop"></i>Technologies</a></span>
					                    <span><i class="ti-location-pin"></i>New York, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					        </div>
					      </div>
							</div><!-- tab-pane -->

							<div role="tabpanel" class="tab-pane fade" id="recent-jobs">
								<div class="row">
					        <div class="col-md-12">
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-1.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Need a web designer</a><span class="full-time">Full-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-brush"></i>Art/Design</a></span>
					                    <span><i class="ti-location-pin"></i>Washington, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-2.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Front-end developer needed</a><span class="full-time">Full-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-desktop"></i>Technologies</a></span>
					                    <span><i class="ti-location-pin"></i>Cupertino, CA, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-3.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Senior Accountant</a><span class="part-time">Part-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-home"></i>Finance</a></span>
					                    <span><i class="ti-location-pin"></i>Delaware, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-4.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Fullstack web developer needed</a><span class="full-time">Full-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-desktop"></i>Technologies</a></span>
					                    <span><i class="ti-location-pin"></i>New York, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					        </div>
					      </div>
							</div><!-- tab-pane -->

							<div role="tabpanel" class="tab-pane fade active in" id="popular-jobs">
								<div class="row">
					        <div class="col-md-12">
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-1.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Need a web designer</a><span class="full-time">Full-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-brush"></i>Art/Design</a></span>
					                    <span><i class="ti-location-pin"></i>Washington, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-2.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Front-end developer needed</a><span class="full-time">Full-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-desktop"></i>Technologies</a></span>
					                    <span><i class="ti-location-pin"></i>Cupertino, CA, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-3.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Senior Accountant</a><span class="part-time">Part-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-home"></i>Finance</a></span>
					                    <span><i class="ti-location-pin"></i>Delaware, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					          <div class="job-list">
					            <div class="thumb">
					              <a href="job-details.html"><img src="{{ ('assets/front-end/img/jobs/img-4.jpg') }}" alt=""></a>
					            </div>
					            <div class="job-list-content">
					              <h4><a href="job-details.html">Fullstack web developer needed</a><span class="full-time">Full-Time</span></h4>
					              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
					              <div class="job-tag">
					                <div class="pull-left">
					                  <div class="meta-tag">
					                    <span><a href="browse-categories.html"><i class="ti-desktop"></i>Technologies</a></span>
					                    <span><i class="ti-location-pin"></i>New York, USA</span>
					                    <span><i class="ti-time"></i>60/Hour</span>
					                  </div>
					                </div>
					                <div class="pull-right">
					                  <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
					                </div>
					              </div>
					            </div>
					          </div>
					        </div>
					      </div>
							</div><!-- tab-pane -->
						</div><!-- tab-content -->
			      <div class="text-center">
			      	<a class="" href="#" style="color:#d81b60;font-weight:600;">Show All</a>
			      </div>
					</div>
		    </div>
		    <div class="col-xs-12 col-sm-3">
		    	<div class="job-alert-widget row">
		    		<div class="job-alert-title col-sm-12">
		    			<h3 class="noBorder_large_title" style="padding:0">Get best matched jobs on your email. No registration needed</h3>
		    		</div>
		    		<div class="col-sm-12 job-alert-button text-center">
			    		<a href="#" class="btn btn-lg btn-auth">
			    			Create a Job Alert
			    		</a>
			    	</div>
	    		</div>

	    		<div class="job-alert-widget row">
	    			<div class="widget-title">
	    				<h3>Premium Designations</h3>
	    			</div>
		    		<ul> 
		    			<li><a title="Chief Executive Officer Jobs" href="" target="_blank">CEO Jobs</a></li> 
		    			<li><a title="Chief Financial Officer Jobs" href="" target="_blank">CFO Jobs</a></li> 
		    			<li><a title="Chief Marketing Officer Jobs" href="" target="_blank">CMO Jobs</a></li> 
		    			<li><a title="Chief Operating Officer Jobs" href="" target="_blank">COO Jobs</a></li> 
		    			<li><a title="Chief Technology Officer Jobs" href="" target="_blank">CTO Jobs</a></li> 
		    		</ul>
		      </div>

		      <div class="job-alert-widget row">
	    			<div class="widget-title">
	    				<h3>Jobs by Category</h3>
	    			</div>
		    		<ul> 
		    			<li>
		    				<a title="Graphic Designer Jobs" href="" target="_blank">
		    					<span>Graphic Designer Jobs</span>
		    					<b class="job-count">4</b>
		    				</a>
		    			</li> 
		    			<li>
		    				<a title="Engineering Jobs" href="" target="_blank">
		    					<span>Engineering Jobs</span>
		    					<b class="job-count">8</b>
		    				</a>
		    			</li> 
		    			<li>
		    				<a title="Mainframe Jobs" href="" target="_blank">
		    					<span>Mainframe Jobs</span>
		    					<b class="job-count">9</b>
		    				</a>
		    			</li> 
		    			<li>
		    				<a title="Legal Jobs" href="" target="_blank">
		    					<span>Legal Jobs</span>
		    					<b class="job-count">8</b>
		    				</a>
		    			</li> 
		    			<li>
		    				<a title="Information Technology Jobs" href="" target="_blank">
		    					<span>IT Jobs</span>
		    					<b class="job-count">19</b>
		    				</a>
		    			</li> 
		        </ul>
		      </div>

		      <div class="job-alert-widget row">
	    			<div class="widget-title">
	    				<h3>Jobs by Location</h3>
	    			</div>
		    		<ul> 
		    			<li>
		    				<a title="Graphic Designer Jobs" href="" target="_blank">
		    					<span>Nagpur</span>
		    					<b class="job-count">4</b>
		    				</a>
		    			</li> 
		    			<li>
		    				<a title="Engineering Jobs" href="" target="_blank">
		    					<span>Pune</span>
		    					<b class="job-count">8</b>
		    				</a>
		    			</li> 
		    			<li>
		    				<a title="Mainframe Jobs" href="" target="_blank">
		    					<span>Mumbai</span>
		    					<b class="job-count">9</b>
		    				</a>
		    			</li> 
		    			<li>
		    				<a title="Legal Jobs" href="" target="_blank">
		    					<span>Hyderabad</span>
		    					<b class="job-count">8</b>
		    				</a>
		    			</li> 
		    			<li>
		    				<a title="Information Technology Jobs" href="" target="_blank">
		    					<span>Bangalore</span>
		    					<b class="job-count">19</b>
		    				</a>
		    			</li> 
		        </ul>
		      </div>
		    </div>
	    </div>
  	</section>
@stop

@section('js')
	<script type="text/javascript">
  	$('document').ready(function(){
    	if($('nav').hasClass('affix')){
    		alert('dasdsa');
    		$('.slicknav_btn slicknav_collapsed').css('margin-right', '30px');
    	}
  	})
  </script>
@stop
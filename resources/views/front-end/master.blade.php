<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="Jobboard">
    
    <title>Hiring99</title>    

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/front-end/logo/tab-logo.png') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front-end/css/bootstrap.min.css') }}" type="text/css">    
    <link rel="stylesheet" href="{{ asset('assets/front-end/css/jasny-bootstrap.min.css') }}" type="text/css">  
    <link rel="stylesheet" href="{{ asset('assets/front-end/css/bootstrap-select.min.css') }}" type="text/css">  
    <!-- Material CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front-end/css/material-kit.css') }}" type="text/css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front-end/fonts/font-awesome.min.css') }}" type="text/css"> 
    <link rel="stylesheet" href="{{ asset('assets/front-end/fonts/themify-icons.css') }}"> 

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front-end/extras/animate.css') }}" type="text/css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('assets/front-end/extras/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/front-end/extras/owl.theme.css') }}" type="text/css">
    <!-- Rev Slider CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front-end/extras/settings.css') }}" type="text/css"> 
    <!-- Slicknav js -->
    <link rel="stylesheet" href="{{ asset('assets/front-end/css/slicknav.css') }}" type="text/css">
    <!-- Main Styles -->
    <link rel="stylesheet" href="{{ asset('assets/front-end/css/main.css') }}" type="text/css">
    <!-- Responsive CSS Styles -->
    <link rel="stylesheet" href="{{ asset('assets/front-end/css/responsive.css') }}" type="text/css">

    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front-end/css/colors/red.css') }}" media="screen" />

    <!-- Carousal  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front-end/css/carousal.css') }}" media="screen" />

    @yield('css')
    
  </head>

  <body>  
    <!-- Header Section Start -->
    @yield('header')
    <!-- Header Section Ends -->
    
    <!-- Find Job Section Start -->
    @yield('content')
    <!-- Find Job Section End -->

    
    
    
    <footer>
    	<!-- Footer Area Start -->
    	<section class="footer-Content">
    		<div class="container">
    			<div class="row">
    				{{-- <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="widget">
                <h3 class="block-title">
                	<img src="{{ ('assets/front-end/logo/logo.png') }}" class="img-responsive" alt="Footer Logo">
                </h3>
                <div class="textwidget">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, consectetur eget facilisis sed.</p>
                </div>
              </div>
            </div> --}}
            <div class="col-md-3 col-sm-6 col-xs-12">
    					<div class="widget">
    						<h3 class="block-title">Popular Categories</h3>
  							<ul class="menu">
                  <li><a href="#">IT and Telecom courses</a></li>
                  <li><a href="#">Banking &amp; Finance courses</a></li>
                  <li><a href="#">Marketing courses</a></li>
                  <li><a href="#">HR courses</a></li>
                </ul>
    					</div>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12">
    					<div class="widget">
    						<h3 class="block-title">Quick Links</h3>
  							<ul class="menu">
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Support</a></li>
                  <li><a href="#">License</a></li>
                  <li><a href="#">Terms & Conditions</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
    					</div>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12">
    					<div class="widget">
                <h3 class="block-title">Trending Jobs</h3>
                <ul class="menu">
                  <li><a href="#">Android Developer</a></li>
                  <li><a href="#">Senior Accountant</a></li>
                  <li><a href="#">Frontend Developer</a></li>
                  <li><a href="#">Junior Tester</a></li>
                  <li><a href="#">Project Manager</a></li>
                </ul>
              </div>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12">
    					<div class="widget">
    						<h3 class="block-title">Follow Us</h3>
                <div class="bottom-social-icons social-icon">  
                  <a class="twitter" href="https://twitter.com/GrayGrids"><i class="ti-twitter-alt"></i></a>
                  <a class="facebook" href="https://web.facebook.com/GrayGrids"><i class="ti-facebook"></i></a>                   
                  <a class="youtube" href="https://youtube.com"><i class="ti-youtube"></i></a>
                  <a class="dribble" href="https://dribbble.com/GrayGrids"><i class="ti-dribbble"></i></a>
                  <a class="linkedin" href="https://www.linkedin.com/GrayGrids"><i class="ti-linkedin"></i></a>
                </div>	
                <p>Join our mailing list to stay up to date and get notices about our new releases!</p>
                <form class="subscribe-box">
                  <input type="text" placeholder="Your email">
                  <input type="submit" class="btn-system" value="Send">
                </form>	
    					</div>
    				</div>
    			</div>
    		</div>
    	</section>
    	<!-- Footer area End -->
    	{{-- <section class="container clients section">
      	<div class="c-carousel">
  			  <div class="c-carousel__arrow c-carousel__arrow--left">
  			  	<i class="fa fa-angle-left"></i>
  			  </div>
  			  <div class="c-carousel__arrow c-carousel__arrow--right">
  			  	<i class="fa fa-angle-right"></i>
  			  </div>
  			  <ul class="c-carousel__slides">
  	    		<li class="carousel__slides_li"><img src="{{ asset('assets/front-end/img/carousal/company1.jpg') }}" alt=""></li>
  	    		<li class="carousel__slides_li"><img src="{{ asset('assets/front-end/img/carousal/company2.jpg') }}" alt=""></li>
  	    		<li class="carousel__slides_li"><img src="{{ asset('assets/front-end/img/carousal/company3.jpg') }}" alt=""></li>
  	    		<li class="carousel__slides_li"><img src="{{ asset('assets/front-end/img/carousal/company4.jpg') }}" alt=""></li>
  	    		<li class="carousel__slides_li"><img src="{{ asset('assets/front-end/img/carousal/company5.jpg') }}" alt=""></li>
  	    		<li class="carousel__slides_li"><img src="{{ asset('assets/front-end/img/carousal/company6.jpg') }}" alt=""></li>
  	    		<li class="carousel__slides_li"><img src="{{ asset('assets/front-end/img/carousal/company7.jpg') }}" alt=""></li>
  			  </ul>
  			</div>
      </section> --}}
    	<!-- Copyright Start  -->
    	<div id="copyright">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
              <div class="site-info text-center">
                <p>All Rights reserved &copy; 2017 - Designed & Developed by <a rel="nofollow" href="http://graygrids.com">GrayGrids</a></p>
              </div>   
    				</div>
    			</div>
    		</div>
    	</div>
    	<!-- Copyright End -->

    </footer>
    <!-- Footer Section End -->  
    
    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="ti-arrow-up"></i>
    </a>

    <div id="loading">
      <div id="loading-center">
        <div id="loading-center-absolute">
          <div class="object" id="object_one"></div>
          <div class="object" id="object_two"></div>
          <div class="object" id="object_three"></div>
          <div class="object" id="object_four"></div>
          <div class="object" id="object_five"></div>
          <div class="object" id="object_six"></div>
          <div class="object" id="object_seven"></div>
          <div class="object" id="object_eight"></div>
        </div>
      </div>
    </div>

    {{-- Register Modal --}}
    <div class="modal fade" id="register" role="dialog">
	    <div class="modal-dialog modal-sm hidden-xs">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Create an account</h4>
	        </div>
	        <div class="modal-body">
        		<form name="" action="" method="post">
        			<div class="col-sm-12"> 
        				<p class="text-center p-tag">Choose account type:</p>
	        			<a href="{{ url('registration') }}">
                  <div class="col-sm-6">
  	      					<div class="box text-center" id="candidate">
  	      						<i class="fa fa-user-o"></i>
  	      					</div>
  	      					<p class="text-center p-tag">Candidate</p>
  		        		</div>
                </a>
		        		<div class="col-sm-6">
	      					<div class="box text-center" id="employer">
	      						<i class="fa fa-building-o"></i>
	      					</div>
	      					<p class="text-center p-tag">Recruiter</p>
		        		</div>
		        	</div>
        		</form>
	        </div>
	        <div class="clearfix"></div>
	        <div class="modal-footer">
	          
	        </div>
	      </div>
	      
	    </div>
	  </div>
        
	  {{-- Login Modal --}}
	  <div class="modal fade" id="login" role="dialog">
	    <div class="modal-dialog modal-sm hidden-xs">
	      <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			        <h4 class="modal-title">Login with</h4>
			    </div>
					<div class="modal-body">
				    <div class="bottom-social-icons social-icon text-center">  
              <a class="twitter" href="https://twitter.com/GrayGrids"><i class="ti-twitter-alt"></i></a>
              <a class="facebook" href="https://web.facebook.com/GrayGrids"><i class="ti-facebook"></i></a>                   
              <a class="youtube" href="https://youtube.com"><i class="ti-youtube"></i></a>
              <a class="dribble" href="https://dribbble.com/GrayGrids"><i class="ti-dribbble"></i></a>
              <a class="linkedin" href="https://www.linkedin.com/GrayGrids"><i class="ti-linkedin"></i></a>
            </div>
						
						<div class="content">
					    <div class="division text-center">
				        <div class="line l"></div>
				        	<span>or</span>
				        <div class="line r"></div>
					    </div>
					    <div class="error"></div>
					    <div class="form loginBox">
				        <form method="" action="" accept-charset="UTF-8">
				            <input id="email" class="form-control" type="text" placeholder="Email" name="email">
				            <div class="form-group">
				            	<input id="password" class="form-control" type="password" placeholder="Password" name="password">
				            	<p class="margin-top"><a >Forgot Password?</a></p>
				            </div>
			            	<input type="submit" class="login-submit form-control" value="Login">
				        </form>
					    </div>
						</div>
					</div>
					<div class="modal-footer">
						<center>Not a member as yet? <a>Register Now</a></center>
					</div>
				</div>
	    </div>
	  </div>


    <!-- Main JS  -->
    <script type="text/javascript" src="{{ asset('assets/front-end/js/jquery-min.js') }}"></script>      
    <script type="text/javascript" src="{{ asset('assets/front-end/js/bootstrap.min.js') }}"></script>    
    <script type="text/javascript" src="{{ asset('assets/front-end/js/material.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/material-kit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/jquery.parallax.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/jquery.slicknav.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/main.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/jasny-bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/bootstrap-select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/form-validator.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/contact-form-script.js') }}"></script>    
    <script type="text/javascript" src="{{ asset('assets/front-end/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front-end/js/carousal.js') }}"></script>



    @yield('js')
    
  </body>
</html>
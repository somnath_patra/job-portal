@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="col-md-4 text-light-blue">
                                <span style="font-size:25px">Add Skills</span>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"><span class="required" style="float:right;">* Fields required</span></div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route('skills/create_action')}}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Skill Title &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorSkill"></span>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="text" placeholder="Skill Title" class="form-control" name="title" id="title">
                                    </div>
                                    
                                </div>
                                </div>
                                  
                              </div>
                                
                                                                
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit" onclick="return validationAdd();">Submit</button>
                                    <a href="{{ route('skills/list') }}" class="btn btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('assets/admin/custom-js/skill.js') }}"></script>
 @stop
  

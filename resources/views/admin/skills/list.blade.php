@extends('admin/welcome')
@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header with-border">
                    <div class="col-md-5 text-light-blue">
                        <span style="font-size:25px"><i class="fa fa-users"></i>&nbsp;&nbsp;Skills List</span>
                    </div>

                    <div class="col-md-7 text-right" style="padding: 0px;">
                        <div>
							<a class="btn btn-primary" title="Export Employee" href=""><i class="glyphicon glyphicon-export"></i>&nbsp;Export To Excel</a>
                            <a class="btn btn-primary" title="Download Format" download="employee.xls" href=""><i class="glyphicon glyphicon-download "></i>&nbsp;Download Format</a>
                            <a class="btn btn-primary" data-toggle="modal" data-target="#upload_employee_modal"><i class="glyphicon glyphicon-import "></i>&nbsp;Import from Excel</a>                             
                            <a class="btn btn-primary" href="{{ route('skills/create') }}" onclick="showloader();">Create</a> 
                        </div>
                        <div class="clearfix">&nbsp;</div>
                         
                    </div>  

                    <div class="col-md-12" style="text-align:center">
                        <span class="msghide"></span>
                    </div>
                    <!-- <div class="clearfix"></div>             -->
                </div>
                <div class="box-body">
                    <div class="">
                        <table id="skill_table" class="table table-striped table-bordered table-hover">
                            <thead class="table-head">
                                <tr>
                                    
                                    <th>Title</th>                                   
                                    <th>Status</th>
                                    
                                </tr>
                            </thead>                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer_js')
<script type="text/javascript">
$(document).ready(function() {
     $('#skill_table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('skills/ajaxSkillList') }}",
        "columns":[
            { "data": "title" },
            { "data": "status" }
        ]
     });
});
</script>
@stop


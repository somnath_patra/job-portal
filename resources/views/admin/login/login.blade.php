<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Hiring99</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/AdminLTE.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('assets/admin/plugins/iCheck/square/blue.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/custom-css/style.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css">
    .form-group {
    margin-bottom: 5px;
}
  </style>
</head>
<body class="hold-transition login-page">
<div class="preloader" style="display: none;background-image: url('{{ asset('assets/admin/dist/img/loader.gif') }}');"></div>
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Admin</b>&nbsp;HIRING</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>     
    <form action="{{ route('login') }}" method="post"> 
     {{ csrf_field() }}   
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
        <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email" autocomplete="off" autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <span class="required_color" id="email_error">&nbsp;</span>       
          @if ($errors->has('email'))          
           <span class="help-block required_color" id="email_error">
             <strong>{{ $errors->first('email') }}</strong>
           </span>
          @endif
      </div>
      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
        <input id="password" type="password" class="form-control" name="password"  placeholder="Password" autocomplete="off">
         <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="required_color" id="password_error">&nbsp;</span>
          @if ($errors->has('password'))
          <span class="required_color help-block" id="password_error">
            <strong>{{ $errors->first('password') }}</strong>
           </span>
          @endif
       
        
      </div>
      <div class="row">        
        <!-- /.col -->
        <div class="col-lg-3">
          
        </div>        
           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary btn-block btn-flat" onclick="return loginValidation();">Sign In</button>
           </div>
        <div class="col-lg-3">
          
        </div>
       
       
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{ asset('assets/admin/custom-js/loginValidation.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('assets/admin/plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
